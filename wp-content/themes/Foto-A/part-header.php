<!-- Begin Top -->
	<section class="top_1" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<div class="small-12 medium-6 columns">
				<?php dynamic_sidebar( 'top_1' ); ?>
			</div>
		</div>
	</section>
	<section class="top" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<div class="small-12 medium-3 columns">
				<?php dynamic_sidebar( 'logo' ); ?>
			</div>
			<div class="small-12 medium-7 columns">
				<?php get_template_part( 'part', 'menu' ); ?>
			</div>
			<div class="small-12 medium-1 columns">
				<?php dynamic_sidebar( 'social_media' ); ?>
			</div>
			<div class="small-12 medium-1 columns">
				<?php dynamic_sidebar( 'cart' ); ?>
			</div>
		</div>
	</section>
<!-- End Top -->